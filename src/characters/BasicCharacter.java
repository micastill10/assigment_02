package characters;

import java.awt.Image;

import game.Main;
import objects.GameObject;
import utils.Res;
import utils.Utils;

public class BasicCharacter implements Character {

    public static final int PROXIMITY_MARGIN = 10;
    private int width, height;
    private int x, y;
    protected boolean moving;
    protected boolean toRight;
    public int counter;
    protected boolean alive;

    public BasicCharacter(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.height = height;
        this.width = width;
        this.counter = 0;
        this.moving = false;
        this.toRight = true;
        this.alive = true;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getCounter() {
        return counter;
    }

    public boolean isAlive() {
        return alive;
    }

    public boolean isMoving() {
        return moving;
    }

    public boolean isToRight() {
        return toRight;
    }

    public void setAlive() {
        this.alive = false;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setMoving(boolean moving) {
        this.moving = moving;
    }

    public void setToRight(boolean toRight) {
        this.toRight = toRight;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public Image walk(String name, int frequency) {
        String str = Res.IMG_BASE + name + (!this.moving || ++this.counter % frequency == 0 ? Res.IMG_STATUS_ACTIVE : Res.IMG_STATUS_NORMAL) +
                (this.toRight ? Res.IMG_DIRECTION_DX : Res.IMG_DIRECTION_SX) + Res.IMG_EXT;
        return Utils.getImage(str);
    }

    public void move() {
        if (Main.scene.getPos() >= 0) {
            this.x = this.x - Main.scene.getMov();
        }
    }

    public boolean hitAhead(GameObject og) {
        if (this.x + this.width < og.getX() || this.x + this.width > og.getX() + 5 ||
                this.y + this.height <= og.getY() || this.y >= og.getY() + og.getHeight()) {
            return false;
        } else
            return true;
    }

    protected boolean hitBack(GameObject og) {
        if (this.x > og.getX() + og.getWidth() || this.x + this.width < og.getX() + og.getWidth() - 5 ||
                this.y + this.height <= og.getY() || this.y >= og.getY() + og.getHeight()) {
            return false;
        } else
            return true;
    }

    protected boolean hitBelow(GameObject og) {
        if (this.x + this.width < og.getX() + 5 || this.x > og.getX() + og.getWidth() - 5 ||
                this.y + this.height < og.getY() || this.y + this.height > og.getY() + 5) {
            return false;
        } else
            return true;
    }

    protected boolean hitAbove(GameObject og) {
        if (this.x + this.width < og.getX() + 5 || this.x > og.getX() + og.getWidth() - 5 ||
                this.y < og.getY() + og.getHeight() || this.y > og.getY() + og.getHeight() + 5) {
            return false;
        } else return true;
    }

    protected boolean hitAhead(BasicCharacter character) {
        if (this.isToRight() == true) {
            if (this.x + this.width < character.getX() || this.x + this.width > character.getX() + 5 ||
                    this.y + this.height <= character.getY() || this.y >= character.getY() + character.getHeight()) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    protected boolean hitBack(BasicCharacter character) {
        if (this.x > character.getX() + character.getWidth() || this.x + this.width < character.getX() + character.getWidth() - 5 ||
                this.y + this.height <= character.getY() || this.y >= character.getY() + character.getHeight())
            return false;
        return true;
    }

    public boolean hitBelow(BasicCharacter character) {
        if (this.x + this.width < character.getX() || this.x > character.getX() + character.getWidth() ||
                this.y + this.height < character.getY() || this.y + this.height > character.getY())
            return false;
        return true;
    }

    public boolean isNearby(BasicCharacter character) {
        if ((this.x > character.getX() - PROXIMITY_MARGIN && this.x < character.getX() + character.getWidth() + PROXIMITY_MARGIN)
                || (this.x + this.width > character.getX() - PROXIMITY_MARGIN && this.x + this.width < character.getX() + character.getWidth() + PROXIMITY_MARGIN))
            return true;
        return false;
    }

    public boolean isNearby(GameObject obj) {
        if ((this.x > obj.getX() - PROXIMITY_MARGIN && this.x < obj.getX() + obj.getWidth() + PROXIMITY_MARGIN) ||
                (this.getX() + this.width > obj.getX() - PROXIMITY_MARGIN && this.x + this.width < obj.getX() + obj.getWidth() + PROXIMITY_MARGIN))
            return true;
        return false;
    }
}
