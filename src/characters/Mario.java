package characters;

import game.Main;
import objects.GameObject;
import objects.Piece;
import utils.Res;
import utils.Utils;

import java.awt.*;

public class Mario extends BasicCharacter {

    public static final int MARIO_OFFSET_Y_INITIAL = 243;
    public static final int FLOOR_OFFSET_Y_INITIAL = 293;
    public static final int WIDTH = 28;
    public static final int HEIGHT = 50;
    public static final int JUMPING_LIMIT = 42;

    private boolean jumping;
    private int jumpingExtent;

    public Mario(int x, int y) {
        super(x, y, WIDTH, HEIGHT);
        Image imgMario = Utils.getImage(Res.IMG_MARIO_DEFAULT);
        this.jumping = false;
        this.jumpingExtent = 0;
    }

    public boolean isJumping() {
        return jumping;
    }

    public void setJumping() {
        this.jumping = true;
    }

    public Image doJump() {
        String str;

        this.jumpingExtent++;

        if (this.jumpingExtent < JUMPING_LIMIT) {
            if (this.getY() > Main.scene.getHeightLimit())
                this.setY(this.getY() - 4);
            else this.jumpingExtent = JUMPING_LIMIT;

            str = this.isToRight() ? Res.IMG_MARIO_SUPER_DX : Res.IMG_MARIO_SUPER_SX;
        } else if (this.getY() + this.getHeight() < Main.scene.getFloorOffsetY()) {
            this.setY(this.getY() + 1);
            str = this.isToRight() ? Res.IMG_MARIO_SUPER_DX : Res.IMG_MARIO_SUPER_SX;
        } else {
            str = this.isToRight() ? Res.IMG_MARIO_ACTIVE_DX : Res.IMG_MARIO_ACTIVE_SX;
            this.jumping = false;
            this.jumpingExtent = 0;
        }

        return Utils.getImage(str);
    }

    public void contact(GameObject obj) {
        if (this.hitAhead(obj) && this.isToRight() || this.hitBack(obj) && !this.isToRight()) {
            Main.scene.setMov(0);
            this.setMoving(false);
        }

        if (this.hitBelow(obj) && this.jumping) {
            Main.scene.setFloorOffsetY(obj.getY());
        } else if (!this.hitBelow(obj)) {
            Main.scene.setFloorOffsetY(FLOOR_OFFSET_Y_INITIAL);
            if (!this.jumping) {
                this.setY(MARIO_OFFSET_Y_INITIAL);
            }

            if (hitAbove(obj)) {
                Main.scene.setHeightLimit(obj.getY() + obj.getHeight()); // the new sky goes below the object
            } else if (!this.hitAbove(obj) && !this.jumping) {
                Main.scene.setHeightLimit(0); // initial sky
            }
        }
    }

    public boolean contactPiece(Piece piece) {
        if (this.hitBack(piece) || this.hitAbove(piece) || this.hitAhead(piece)
                || this.hitBelow(piece))
            return true;

        return false;
    }

    public void contact(BasicCharacter character) {
        if (this.hitAhead(character) || this.hitBack(character)) {
            if (character.alive) {
                this.setMoving(false);
                this.setAlive();
            } else this.alive = true;
        } else if (this.hitBelow(character)) {
            character.setMoving(false);
            character.setAlive();
        }
    }
}
