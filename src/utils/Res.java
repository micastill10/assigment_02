package utils;

import game.AudioPrincipalScala;

/**
 * @author Roberto Casadei
 */

public class Res {
    public static final String IMG_BASE = "/resources/images/";
    public static final String AUDIO_BASE = "/resources/audio/";
    public static final String IMG_EXT = ".png";

    public static final String IMG_STATUS_NORMAL = "";
    public static final String IMG_STATUS_ACTIVE = "A";
    public static final String IMG_STATUS_DEAD = "E";
    public static final String IMG_STATUS_IDLE = "F";
    public static final String IMG_STATUS_SUPER = "S";

    public static final String IMG_DIRECTION_SX = "G";
    public static final String IMG_DIRECTION_DX = "D";

    public static final String IMG_CHARACTER_MUSHROOM = "mushroom";
    public static final String IMG_CHARACTER_TURTLE = "turtle";
    public static final String IMG_CHARACTER_MARIO = "mario";

    public static final String IMG_OBJECT_BLOCK = "block";
    public static final String IMG_OBJECT_PIECE1 = "piece1";
    public static final String IMG_OBJECT_PIECE2 = "piece2";
    private static final String IMG_OBJECT_TUNNEL = "tunnel";

    public static final String IMG_MARIO_DEFAULT = IMG_BASE + IMG_CHARACTER_MARIO + IMG_DIRECTION_DX + IMG_EXT;
    public static final String IMG_MARIO_SUPER_SX = IMG_BASE + IMG_CHARACTER_MARIO + IMG_STATUS_SUPER + IMG_DIRECTION_SX + IMG_EXT;
    public static final String IMG_MARIO_SUPER_DX = IMG_BASE + IMG_CHARACTER_MARIO + IMG_STATUS_SUPER + IMG_DIRECTION_DX + IMG_EXT;
    public static final String IMG_MARIO_ACTIVE_SX = IMG_BASE + IMG_CHARACTER_MARIO + IMG_STATUS_ACTIVE + IMG_DIRECTION_DX + IMG_EXT;
    public static final String IMG_MARIO_ACTIVE_DX = IMG_BASE + IMG_CHARACTER_MARIO + IMG_STATUS_ACTIVE + IMG_DIRECTION_DX + IMG_EXT;
    public static final String IMG_MARIO_SX = IMG_BASE + IMG_CHARACTER_MARIO + IMG_STATUS_NORMAL + IMG_DIRECTION_DX + IMG_EXT;
    public static final String IMG_MARIO_DX = IMG_BASE + IMG_CHARACTER_MARIO + IMG_STATUS_NORMAL + IMG_DIRECTION_DX + IMG_EXT;

    public static final String IMG_MUSHROOM_DX = IMG_BASE + IMG_CHARACTER_MUSHROOM + IMG_STATUS_NORMAL + IMG_DIRECTION_DX + IMG_EXT;
    public static final String IMG_MUSHROOM_SX = IMG_BASE + IMG_CHARACTER_MUSHROOM + IMG_STATUS_NORMAL + IMG_DIRECTION_SX + IMG_EXT;
    public static final String IMG_MUSHROOM_DEAD_DX = IMG_BASE + IMG_CHARACTER_MUSHROOM + IMG_STATUS_DEAD + IMG_DIRECTION_DX + IMG_EXT;
    public static final String IMG_MUSHROOM_DEAD_SX = IMG_BASE + IMG_CHARACTER_MUSHROOM + IMG_STATUS_DEAD + IMG_DIRECTION_SX + IMG_EXT;
    public static final String IMG_MUSHROOM_DEFAULT = Res.IMG_BASE + Res.IMG_CHARACTER_MUSHROOM + Res.IMG_STATUS_ACTIVE + IMG_DIRECTION_DX + IMG_EXT;

    public static final String IMG_TURTLE_IDLE = IMG_BASE + IMG_CHARACTER_TURTLE + IMG_STATUS_IDLE + IMG_EXT;
    public static final String IMG_TURTLE_DEAD = IMG_TURTLE_IDLE;

    public static final String IMG_BLOCK = IMG_BASE + IMG_OBJECT_BLOCK + IMG_EXT;

    public static final String IMG_PIECE1 = IMG_BASE + IMG_OBJECT_PIECE1 + IMG_EXT;
    public static final String IMG_PIECE2 = IMG_BASE + IMG_OBJECT_PIECE2 + IMG_EXT;
    public static final String IMG_TUNNEL = IMG_BASE + IMG_OBJECT_TUNNEL + IMG_EXT;

    public static final String IMG_BACKGROUND = IMG_BASE + "background" + IMG_EXT;
    public static final String IMG_CASTLE = IMG_BASE + "castleIni" + IMG_EXT;
    public static final String START_ICON = IMG_BASE + "start" + IMG_EXT;
    public static final String IMG_CASTLE_FINAL = IMG_BASE + "castle" + IMG_EXT;
    public static final String IMG_FLAG = IMG_BASE + "flag" + IMG_EXT;

    public static final String AUDIO_MONEY = AUDIO_BASE + "money.wav";
    public static final String AUDIO_FOND = AUDIO_BASE + "song.wav";
    // Call from Object Scala
    public void sond2() {
        AudioPrincipalScala.playSound("/resources/audio/song.wav");
    }

}



