package game;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

public class AudioPrincipal {
    private Clip clip2;

    public AudioPrincipal(String son2) {

        try {
            AudioInputStream audio = AudioSystem.getAudioInputStream(getClass().getResource(son2));
            clip2 = AudioSystem.getClip();
            clip2.open(audio);
            clip2.loop(clip2.LOOP_CONTINUOUSLY);
        } catch (Exception e) {
            // TODO: log error
        }
    }

    public Clip getClip2() {
        return clip2;
    }

    public void play() { clip2.start(); }

    public void stop() { clip2.stop();  }

    public static void playSound(String son2) {
        AudioPrincipalScala Ap = new AudioPrincipalScala(son2);
        Ap.play();
      //  Ap.clip2.loop(Clip.LOOP_CONTINUOUSLY);

    }

}
