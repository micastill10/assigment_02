package game;

public class Refresh implements Runnable {

    public void run() {
        while (true) {
            Main.scene.repaint();
            try {
                int PAUSE = 3;
                Thread.sleep(PAUSE);
            } catch (InterruptedException e) {
            }
        }
    }

} 
