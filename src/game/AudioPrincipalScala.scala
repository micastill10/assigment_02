package game

import javax.sound.sampled.AudioInputStream
import javax.sound.sampled.AudioSystem
import javax.sound.sampled.Clip
import scala.beans.{BeanProperty, BooleanBeanProperty}

object AudioPrincipalScala {
  def playSound(son2: String): Unit = {
    val Ap: AudioPrincipalScala = new AudioPrincipalScala(son2)
    Ap.play()
    Ap.clip2.loop(Clip.LOOP_CONTINUOUSLY)
  }

}
class AudioPrincipalScala(son2: String) {

  @BeanProperty
  var clip2: Clip = _

  try {
    val audio: AudioInputStream =
      AudioSystem.getAudioInputStream(getClass.getResource(son2))
    clip2 = AudioSystem.getClip
    clip2.open(audio)
    clip2.loop(Clip.LOOP_CONTINUOUSLY)
  } catch {
    case _: Exception => {}

  }

  def play(): Unit = {
    clip2.start()
  }

  def stop(): Unit = {
    clip2.stop()
  }

}







