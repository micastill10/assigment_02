package game;

import utils.Res;

import javax.swing.*;

public class Main {

    public static final int WINDOW_WIDTH = 700;
    public static final int WINDOW_HEIGHT = 360;
    public static final String WINDOW_TITLE = "Super Mario";
    public static Platform scene;

    public static void main(String[] args) {
        JFrame frame = new JFrame(WINDOW_TITLE);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        frame.setLocationRelativeTo(null);
        frame.setResizable(true);
        frame.setAlwaysOnTop(true);

        scene = new Platform();
        frame.setContentPane(scene);
        frame.setVisible(true);

        Thread timer = new Thread(new Refresh());
        timer.start();
       //call from Res
        Res rs = new Res();
        rs.sond2();

        }
    }


