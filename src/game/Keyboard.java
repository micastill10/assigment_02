package game;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Keyboard implements KeyListener {

    @Override
    public void keyPressed(KeyEvent e) {

        if (Main.scene.mario.isAlive() == true) {
            if (e.getKeyCode() == KeyEvent.VK_RIGHT) {

                // do not move the castle and start
                if (Main.scene.getPos() == -1) {
                    Main.scene.setPos(0);
                    Main.scene.setBackground1PosX(-50);
                    Main.scene.setBackground2PosX();
                }
                Main.scene.mario.setMoving(true);
                Main.scene.mario.setToRight(true);
                Main.scene.setMov(1); // moves to the left
            } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {

                if (Main.scene.getPos() == 4601) {
                    Main.scene.setPos(4600);
                    Main.scene.setBackground1PosX(-50);
                    Main.scene.setBackground2PosX();
                }

                Main.scene.mario.setMoving(true);
                Main.scene.mario.setToRight(false);
                Main.scene.setMov(-1); // moves to the right
            }
            // jump
            if (e.getKeyCode() == KeyEvent.VK_UP) {
                Main.scene.mario.setJumping();
                Audio.playSound("/resources/audio/jump.wav");
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        Main.scene.mario.setMoving(false);
        Main.scene.setMov(0);

    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

}
